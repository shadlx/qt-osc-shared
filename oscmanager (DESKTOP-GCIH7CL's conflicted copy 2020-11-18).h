#ifndef OSCMANAGER_H
#define OSCMANAGER_H

#include <QObject>

#include "OSCParser.h"
#include "OSCMessage.h"
#include <QTcpSocket>
#include <QUdpSocket>



class OSCManager: public QObject
{
    Q_OBJECT


public:

    OSCManager() {}
    //void operator = (OSCManager const&) = delete;

    void startUdp(int outPort, int inPort, QHostAddress hostname);
    void startTcp(QString host ="", unsigned int port =0);
    void startTcp(QTcpSocket *socket);
    bool isConnected() const;

    void setIsSLIP(bool isSlip);

    QHostAddress hostName() { return m_hostName; }

    int outPort() { return m_OutPort;}
    int inPort() { return m_InPort;}
    QString outIP() { return m_hostName.toString();}
public slots:
    //void broadcastOSC(OSCPacketWriter *packet);
    void broadcastDatagram(QByteArray datagram);
    void sendOSC(OSCPacketWriter *packet);
    void onConnected();
    void onError();
    void readIncomingTcpStream();
    void sendOSCUdp(QString path, QVariantList arguments, QHostAddress ip, int port, bool intToFloat=false);
    void sendOSCUdpQML(QString path, QVariantList arguments, QString ip, int port, bool intToFloat=false){ sendOSCUdp(path, arguments, QHostAddress(ip), port, intToFloat);}

    void sendMessage(QString path, QVariantList arguments);

private slots:
    void processPendingDatagrams();


signals:
    void publishOSC(const QString&,const OSCArgument*,size_t);
    void messageReceived(OSCMessage msg);
    void packetSent();
    void connectionStatusChanged();

private:
    //OSCManager() {}
    QUdpSocket *m_udpSocket = nullptr;

    QTcpSocket *m_tcpSocket = nullptr;

    OSCStream::EnumFrameMode m_tcpFrameMode = OSCStream::FRAME_MODE_1_0;

    QHostAddress m_hostName;
    int m_InPort;
    int m_OutPort;
    bool m_useTCP;
    bool m_amConnecting = false;

    void addArgsToPacketWriter(QVariantList args, OSCPacketWriter &writer, bool intToFloat=false);


    void startReceiveUdp(int port);
    void startSendUdp(int port);

    /**
     * @brief m_incompleteStreamData may contain the begin of an incomplete OSC packet
     * (from TCP stream)
     */
    QByteArray				m_incompleteStreamData;


    QByteArray popPacketLengthFramedPacketFromStreamData(QByteArray& tcpData) const;
    QByteArray popSlipFramedPacketFromStreamData(QByteArray& tcpData) const;


    void processIncomingRawMessage(QByteArray msgData);
    void processIncomingRawData(QByteArray msgData);
    void sendMessageData(char* packet, size_t outSize);

    void sendMessageDataUdp(char* packet, size_t outSize, QHostAddress dest, int port);

};


#endif // OSCMANAGER_H


