#include <QtNetwork>
#include <QtCore>

#include <QObject>
#include <QDebug>

#include "oscmanager.h"

// http://www.rfc-editor.org/rfc/rfc1055.txt
#define SLIP_END		0xc0    /* indicates end of packet */
#define SLIP_ESC		0xdb    /* indicates byte stuffing */
#define SLIP_ESC_END	0xdc    /* ESC ESC_END means END data byte */
#define SLIP_ESC_ESC	0xdd    /* ESC ESC_ESC means ESC data byte */

#define SLIP_CHAR(x)	static_cast<char>(static_cast<unsigned char>(x))



void OSCManager::startUdp(int outPort, int inPort, QHostAddress hostname)
{
    m_useTCP = false;
    m_hostName = hostname;
    if (m_udpSocket == nullptr)
    {
        m_udpSocket = new QUdpSocket(this);

    }
    if (isConnected())
        m_udpSocket->close();

    emit connectionStatusChanged();

    //always do this even if no port..
    startReceiveUdp(inPort);

    startSendUdp(outPort);



}

void OSCManager::startReceiveUdp(int port)
{
    m_InPort = port;

    //m_receiveUdpSocket = QUdpSocket(this);
    m_udpSocket->bind(m_InPort, QUdpSocket::ShareAddress);
    connect( m_udpSocket, SIGNAL(readyRead()),
             this, SLOT(processPendingDatagrams()));
    // connect(quitButton, SIGNAL(clicked()), this, SLOT(close())); //TODO: SLOT: Close when we close
    qDebug() << "started udp rec" << port;
    emit connectionStatusChanged();
}

void OSCManager::startSendUdp(int port)
{
    m_useTCP = false;
    //m_tcpSocket = nullptr;
    //m_sendUdpSocket = new QUdpSocket(this);

    m_OutPort = port;
    emit connectionStatusChanged();


}

void OSCManager::startTcp(QString hostName, unsigned int port)
{

    //TODO: m_amConnecting is now getting confused..
    m_amConnecting = true;

    m_useTCP = true;

    if (port > 0)
        m_InPort = port;
    if (hostName.length()>1)
        m_hostName = QHostAddress(hostName);
    if (m_tcpSocket)
        m_tcpSocket->abort();
    else
        m_tcpSocket = new QTcpSocket(this);

    if (m_hostName.isNull() || !m_InPort)
    {
        m_amConnecting = false;
        return; //can't do anything
    }

    m_tcpSocket->disconnectFromHost();
    connect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(onError()));

    connect(m_tcpSocket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError()));
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(readIncomingTcpStream()));

    m_tcpSocket->connectToHost(m_hostName, m_InPort);
    emit connectionStatusChanged();

}

void OSCManager::startTcp(QTcpSocket *socket)
{
    m_useTCP = true;
    if (m_tcpSocket)
    {
        m_tcpSocket->abort();
        m_tcpSocket = Q_NULLPTR;
    }

    m_tcpSocket = socket;

    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError()));
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(readIncomingTcpStream()));
    emit connectionStatusChanged();

}

void OSCManager::onConnected()
{
    m_amConnecting = false;
    emit connectionStatusChanged();
}
void OSCManager::onError()
{
    m_amConnecting = false;
    m_tcpSocket->disconnectFromHost();
    emit connectionStatusChanged();
    //abort();

}
void OSCManager::processPendingDatagrams()
{
    //static int count = 0;
    //count ++;

    QByteArray datagram;

    if (m_useTCP) return; //this is only UDP recieve
    while (m_udpSocket->hasPendingDatagrams()) {

        datagram.resize(int(m_udpSocket->pendingDatagramSize()));
        m_udpSocket->readDatagram(datagram.data(), datagram.size());

        char *data = datagram.data();
        QByteArray databuf = QByteArray(reinterpret_cast<char*>(data), datagram.size());

        processIncomingRawMessage(databuf);
    }
}






void OSCManager::sendOSC(OSCPacketWriter *packet)
{
    //qDebug("sending osc");
    size_t size;
    char *data = packet->Create(size);

    if(data && size!=0)
    {

        //        sPacket packet;
        //        packet.data = data;
        //        packet.size = size;

        QByteArray* array = new QByteArray(data, size);

        broadcastDatagram(*array);

        delete[] data;
    }


}


void OSCManager::broadcastDatagram(QByteArray datagram)
{
    //qDebug()<<QHostAddress::Broadcast;
    m_udpSocket->writeDatagram(datagram, QHostAddress::Broadcast, m_OutPort);
}


void OSCManager::readIncomingTcpStream()
{
    QByteArray streamData = m_tcpSocket->readAll();

    // check if there is incomplete stream data left from last call:
    if (!m_incompleteStreamData.isEmpty()) {
        streamData = m_incompleteStreamData + streamData;
    }

    // as long as there is stream data left:
    while (streamData.size()) {
        // try to get a complete packet from the stream data:
        QByteArray packet = (m_tcpFrameMode == OSCStream::FRAME_MODE_1_1) ? popSlipFramedPacketFromStreamData(streamData) : popPacketLengthFramedPacketFromStreamData(streamData);

        if (packet.isEmpty()) {
            // there is no more complete packet
            // but maybe there is incomplete packet data left:
            m_incompleteStreamData = streamData;
            return;
        }

        processIncomingRawData(packet);
    }
}




QByteArray OSCManager::popPacketLengthFramedPacketFromStreamData(QByteArray& tcpData) const
{
    // the first 4 bytes are the length of the OSC message following as an int32:
    int32_t messageLength = reinterpret_cast<int32_t*>(tcpData.data())[0];
    // adjust the network byte order (?):
    OSCArgument::Swap32(&messageLength);

    // check if the messageLength is in the range of a valid message:
    if (messageLength <= 0 || messageLength > 512) {
        // this is not a valid OSC message
        // received data will be discarded:
        qDebug("Invalid data received (message length in TCP packet is out of range). Check Protocol Settings.");
        tcpData.resize(0);
        return QByteArray();
    }

    // check if the message is completely received:
    if (messageLength > tcpData.size()) {
        // the message length is greater than the size of the data received
        // so there is nothing to pop / return yet:
        return QByteArray();
    }

    // create a new QByteArray containing only the OSC message data:
    QByteArray messageData = QByteArray(tcpData.data() + sizeof(messageLength), messageLength);
    // remove the messageLength and the message data from the received data:
    tcpData.remove(0, messageLength + sizeof(messageLength));

    return messageData;
}

//currently only packet length with eos..
QByteArray OSCManager::popSlipFramedPacketFromStreamData(QByteArray& tcpData) const
{
    // A SLIP framed packet begins and ends with a SLIP END character.

    // find first SLIP END character:
    // most probably it is the first character:
    int firstSlipEndPosition = 0;
    if (tcpData[0] != SLIP_CHAR(SLIP_END)) {
        for (int i=1; i<tcpData.size(); ++i) {
            if (tcpData[i] == SLIP_CHAR(SLIP_END)) {
                firstSlipEndPosition = i;
                break;
            }
        }
        if (firstSlipEndPosition == 0) {
            // if first slip end postion is still 0,
            // this means there is no SLIP END character in the data
            // -> discard the data and return nothing:
            //addToLog(false, "[In] Invalid data received (missing SLIP END character). Check Protocol Settings.");
            tcpData.resize(0);
            return QByteArray();
        }
    }

    // find second SLIP END character:
    int secondSlipEndPosition = -1;
    for (int i=firstSlipEndPosition+1; i<tcpData.size(); ++i) {
        if (tcpData[i] == SLIP_CHAR(SLIP_END)) {
            secondSlipEndPosition = i;
            break;
        }
    }
    if (secondSlipEndPosition < 0) {
        // there is no second SLIP END character
        // -> message is not yet complete:
        return QByteArray();
    }

    // at this point we should have found two SLIP END characters
    Q_ASSERT(secondSlipEndPosition > firstSlipEndPosition);

    int messageLength = secondSlipEndPosition - firstSlipEndPosition - 1;
    // create a new QByteArray containing only the OSC message data:
    QByteArray messageData = QByteArray(tcpData.data() + firstSlipEndPosition + 1, messageLength);
    // remove everything before the second SLIP END from the received data:
    tcpData.remove(0, secondSlipEndPosition + 1);

    // replace characters that have been escaped because of SLIP framing:
    messageData.replace(SLIP_CHAR(SLIP_ESC) + SLIP_CHAR(SLIP_ESC_END), SLIP_CHAR(SLIP_END));
    messageData.replace(SLIP_CHAR(SLIP_ESC) + SLIP_CHAR(SLIP_ESC_ESC), SLIP_CHAR(SLIP_ESC));

    return messageData;
}


void OSCManager::processIncomingRawData(QByteArray msgData)
{
    // check if the data is a single message or a bundle of messages:
    if (msgData[0] == '/') {
        // it starts with a "/" -> it is a single message:
        processIncomingRawMessage(msgData);
    } else if (msgData.startsWith("#bundle")) {
        // it starts with "#bundle" -> it is a bundle
        // remove "#bundle" string (8 bytes) and unused timetag (8 bytes):
        msgData.remove(0, 16);
        // try to get all messages in the bundle:
        while (msgData.size()) {
            // each message starts with the length of the message as int32
            // this is the same as a TCP 1.0 packet, so the function for that can be used:
            QByteArray message = popPacketLengthFramedPacketFromStreamData(msgData);
            // process the message:
            processIncomingRawMessage(message);
        }
    } else {
        // invalid data
        qDebug() << "[Invalid] Raw: " << msgData[0] << QString::fromLatin1(msgData.data(), msgData.size());
    }
}

void OSCManager::processIncomingRawMessage(QByteArray msgData)
{
    // build an OSC message from the data:
    OSCMessage msg(msgData);

    // Log if logging of incoming messages is enabled:
    if (!msg.isValid()) {
        qDebug() <<"[Invalid] Raw: " << QString::fromLatin1(msgData.data(), msgData.size());
    }

    // emit message received signal:
    if (msg.isValid()) {
        emit messageReceived(msg);
//        qDebug() << msg.path();
//        qDebug() << msg.arguments();
        //foreach (QVariant v, msg.arguments())
          //  qDebug() << v;
    }
}

void OSCManager::addArgsToPacketWriter(QVariantList arguments, OSCPacketWriter &packetWriter, bool intToFloat)
{

    for (QVariantList::iterator i = arguments.begin(); i!= arguments.end(); i++)
    {
        switch (i->type())
        {
            case QVariant::Bool:
                packetWriter.AddBool(i->toBool());
            break;
            case QVariant::Int:
            case QVariant::UInt:
            case QVariant::Char:
            case QVariant::LongLong:
                if (!intToFloat)
                    packetWriter.AddInt32(i->toInt());
                else
                    packetWriter.AddFloat32(i->toFloat());
            break;
            case QVariant::String:
                packetWriter.AddString(i->toString().toStdString());
            break;
            case QVariant::Double:
            case QMetaType::Float:
                packetWriter.AddFloat32(i->toFloat());
            break;
        default:
            qDebug() << "unsigned osc argument of type " << i->typeName() << i->type() <<  arguments;

        }
    }


}
void OSCManager::sendOSCUdp(QString path, QVariantList arguments, QHostAddress ip, int port, bool intToFloat)
{
    OSCPacketWriter packetWriter(path.toStdString());
    addArgsToPacketWriter(arguments, packetWriter, intToFloat);

    size_t outSize;

    char* packet = packetWriter.Create(outSize);

    sendMessageDataUdp(packet, outSize, ip, port);


}

void OSCManager::sendMessage(QString path, QVariantList arguments)
{
    OSCPacketWriter packetWriter(path.toStdString());

    addArgsToPacketWriter(arguments, packetWriter);
    size_t outSize;

    char* packet = packetWriter.Create(outSize);

    sendMessageData(packet, outSize);

}


void OSCManager::sendMessageData(char* packet, size_t outSize)
{
    //qDebug() << "send";
    // send packet either with UDP or TCP:
    if (m_useTCP) {
        //qDebug() << "sedning with tcp";
        // check if TCP socket is connected:
        if (m_tcpSocket != nullptr && m_tcpSocket->state() == QAbstractSocket::ConnectedState) {
            // socket is connected
            // for TCP transmission the packet has to be framed:
            char* framedPacket = OSCStream::CreateFrame(m_tcpFrameMode, packet, outSize);

            m_tcpSocket->write(framedPacket, outSize);
            delete[] framedPacket;

        }
    } else {

        //qDebug() << "sending message UDP " << m_hostName << m_OutPort << outSize << packet << m_udpSocket;

        // use UDP:

        if (!m_hostName.isNull() && m_OutPort)
        {
           //qDebug() << "sending it";
           sendMessageDataUdp(packet, outSize, m_hostName, m_OutPort);

        }//else
            //qDebug() << "not sending";
    }

    delete[] packet;
    emit packetSent();
}

void OSCManager::sendMessageDataUdp(char* packet, size_t outSize, QHostAddress dest, int port)
{
    if (m_udpSocket == nullptr)
    {
        m_udpSocket = new QUdpSocket(this);

    }

    m_udpSocket->writeDatagram(packet, outSize, dest, port);
}

bool OSCManager::isConnected() const
{
    if (m_useTCP) {
        //qDebug() << "state is " << m_tcpSocket->state();
        return m_tcpSocket != nullptr && m_tcpSocket->state() == QAbstractSocket::ConnectedState;
    } else {
        if (m_udpSocket != nullptr)
            return m_udpSocket->state() ==  QAbstractSocket::BoundState;
        else
            return false;
    }
}

void OSCManager::setIsSLIP(bool isSlip)
{
    m_tcpFrameMode = isSlip ? OSCStream::FRAME_MODE_1_1 : OSCStream::FRAME_MODE_1_0;
}



